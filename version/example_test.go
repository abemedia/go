package version_test

import (
	"fmt"

	"gitlab.alpinelinux.org/alpine/go/version"
)

func Example() {
	v := version.Version("1.2.3-r0")

	nv := &v
	t := version.Digit
	var pt version.Token

	var q int64
	for t != version.End {
		pt = t
		t, nv, q = nv.GetToken(t)
		if pt == version.Digit {
			fmt.Printf("%d\n", q)
		}
		if pt == version.RevisionDigit {
			fmt.Printf("r%d\n", q)
		}
	}

	// Output:
	// 1
	// 2
	// 3
	// r0
}
