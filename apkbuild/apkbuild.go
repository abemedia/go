// Package apkbuild provides functions to get metadata from APKBUILD files. The
// main function Parse() interprets an APKBUILD file to obtain the package
// metadata.
//
// ParseSecfixes() can be used to get the secfixes present in the comments of an
// APKBUILD file.
package apkbuild

import (
	"strings"
	"text/scanner"

	"mvdan.cc/sh/v3/syntax"
)

type (
	// SourceHash represents a single entry in the checksum list.
	//
	//     sha512sums="32de73bec5e2a522b0bb22dda723f31aabee6ed3cb3bab9646f7b598662732d5f9579441bf04607187b99eb054f33db78220176b0fc0cb7a7ee3ce70917f28ff  filename.tar.gz"
	//
	SourceHash struct {
		Source string
		Hash   string
	}

	// Subpackage represents an entry in the subpackage list.
	//
	//     subpackages="subpkgname:splitfunc:arch"
	//
	Subpackage struct {
		Subpkgname string
		SplitFunc  string
		Arch       string
	}

	// Source represents an entry in the sources list.
	//
	//    sources="filename.tar.gz::https://domain.org/file.tar.gz"
	//
	Source struct {
		Filename string
		Location string
	}

	// Options is a list of enabled options.
	//
	//     options="net"
	//
	Options []string

	// Arches is list of supported arches.
	//
	//     arches="all !armhf"
	//
	Arches []string

	// PkgSpec represents a single dependency with its modifiers.
	//
	//     depends="lib>2.0"
	//
	PkgSpec struct {
		Pkgname    string
		Constraint string
		Version    string
		RepoPin    string
		Conflicts  bool
	}

	// PackageSpecs represents list of dependencies.
	//
	//     makedepends="dep1 dep2"
	//
	PackageSpecs []PkgSpec

	// Trigger represents a single trigger, with the scriptname and paths parsed.
	//
	//     scriptname.trigger=path/a:path/b
	//
	Trigger struct {
		Scriptname string
		Paths      []string
	}

	// Triggers represents a list of triggers.
	//
	//     triggers="scriptname.trigger=path/a:path/b otherscript.trigger=path/c:path/e"
	//
	Triggers []Trigger

	// Apkbuild contains all the metadata for a single package.
	Apkbuild struct {
		Pkgname          string
		Pkgver           string
		Pkgrel           string
		Pkgdesc          string
		Url              string
		License          string
		Options          Options
		Arch             Arches
		Depends          PackageSpecs
		DependsDev       PackageSpecs
		Makedepends      PackageSpecs
		MakedependsBuild PackageSpecs
		MakedependsHost  PackageSpecs
		Checkdepends     PackageSpecs
		Install          []string
		Pkgusers         []string
		Pkggroups        []string
		Triggers         Triggers
		Subpackages      []Subpackage
		Source           []Source
		Replaces         []PkgSpec
		Provides         []PkgSpec
		ProviderPriority string
		Builddir         string
		Sha512sums       []SourceHash
		Funcs            map[string]*syntax.Stmt
	}
)

// Has returns whether a specific option is present in the options list.
func (o Options) Has(wanted string) bool {
	for _, option := range o {
		if option == wanted {
			return true
		}
	}
	return false
}

// IsRemote returns whether source location is remote (http, https, ftp).
func (s Source) IsRemote() bool {
	return strings.HasPrefix(s.Location, "http://") ||
		strings.HasPrefix(s.Location, "https://") ||
		strings.HasPrefix(s.Location, "ftp://")
}

// NewArchesFromString splits a list of arches in individual components.
func NewArchesFromString(arches string) Arches {
	return strings.Fields(arches)
}

// Enabled returns whether this package is enabled on a given arch. All arches
// satisfy 'all' and 'nooarch', unless the arch has specifically been disabled.
func (a Arches) Enabled(arch string) bool {
	allArches := false
	for _, archItem := range a {
		switch archItem {
		case "all", "noarch":
			allArches = true
		case arch:
			return true
		default:
			if strings.HasPrefix(archItem, "!") && archItem[1:] == arch {
				return false
			}
		}
	}
	return allArches
}

// NewPackageFromString takes a single token from a dependency list, and parses
// it in each individual component.
func NewPkgSpecFromString(pkg string) (pkgspec PkgSpec) {
	const (
		StateStart = iota
		StateWaitToken
		StateExpectVersion
		StateExpectRepoPin
		StateEnd
	)

	s := scanner.Scanner{}
	s.Init(strings.NewReader(pkg))
	s.Mode = scanner.ScanIdents
	s.IsIdentRune = func(ch rune, i int) bool {
		return !(ch == '<' || ch == '>' || ch == '=' || ch == '!' || ch == '~' || ch == '@') &&
			ch > '!' && ch <= 'z'
	}

	currentState := StateStart
	for tok := s.Scan(); tok != scanner.EOF; tok = s.Scan() {
		tokenText := s.TokenText()

		switch tokenText {
		case "!":
			pkgspec.Conflicts = true
		case "<", ">", "=", "~":
			pkgspec.Constraint += tokenText
			currentState = StateExpectVersion
		case "@":
			currentState = StateExpectRepoPin
		default:
			switch currentState {
			case StateStart:
				pkgspec.Pkgname = tokenText
				currentState = StateWaitToken
			case StateExpectVersion:
				pkgspec.Version = tokenText
				currentState = StateWaitToken
			case StateExpectRepoPin:
				pkgspec.RepoPin = tokenText
				currentState = StateEnd
			}
		}
	}

	return pkgspec
}

// String returns the PkgSpec as a string as it would be present in a dependency
// list.
func (ps PkgSpec) String() string {
	pkgString := strings.Builder{}

	pkgString.WriteString(ps.Pkgname)
	pkgString.WriteString(ps.Constraint)
	pkgString.WriteString(ps.Version)
	if ps.RepoPin != "" {
		pkgString.WriteString("@" + ps.RepoPin)
	}
	return pkgString.String()
}

// NewTriggerFromString takes a single element of a triggers string, parses it,
// an returns a Trigger.
func NewTriggerFromString(rawTrigger string) (trigger Trigger) {
	script, paths, _ := strings.Cut(rawTrigger, "=")
	trigger.Scriptname = script
	trigger.Paths = strings.Split(paths, ":")

	return
}

// NewTriggersFromString takes a triggers string and returns a list of Trigger
// objects.
func NewTriggersFromString(rawTriggers string) (triggers Triggers) {
	for _, elem := range strings.Fields(rawTriggers) {
		triggers = append(triggers, NewTriggerFromString(elem))
	}
	return
}
