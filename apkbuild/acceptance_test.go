package apkbuild_test

import (
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.alpinelinux.org/alpine/go/apkbuild"
	"mvdan.cc/sh/v3/expand"
)

func TestParsePackagerA(t *testing.T) {
	assert := assert.New(t)
	require := require.New(t)

	f, err := os.Open("testdata/package-a/APKBUILD")
	require.Nil(err)

	pkg, err := apkbuild.Parse(apkbuild.NewApkbuildFile("package-a", f), expand.ListEnviron("CARCH=armhf"))
	require.Nil(err)

	assert.Equal("package-a", pkg.Pkgname, "pkgname")
	assert.Equal("1.2.0", pkg.Pkgver, "pkgver")
	assert.Equal("1", pkg.Pkgrel, "pkgrel")
	assert.Equal("package a", pkg.Pkgdesc, "pkgdesc")
	assert.Equal("https://package-a.org/", pkg.Url, "url")
	assert.Equal("MIT", pkg.License, "license")
	require.Len(pkg.Depends, 4, "depends")

	dep1 := pkg.Depends[0]
	assert.Equal("package-b", dep1.Pkgname, "depends package-b")
	assert.False(dep1.Conflicts, "depends package-b")
	assert.Equal("", dep1.Constraint, "depends package-b")
	assert.Equal("", dep1.Version, "depends package-b")
	assert.Equal("", dep1.RepoPin, "depends package-b")

	dep2 := pkg.Depends[1]
	assert.Equal("package-c", dep2.Pkgname, "depends package-c")
	assert.False(dep2.Conflicts, "depends package-c")
	assert.Equal(">", dep2.Constraint, "depends package-c")
	assert.Equal("3", dep2.Version, "depends package-c")
	assert.Equal("", dep2.RepoPin, "depends package-c")

	dep3 := pkg.Depends[2]
	assert.Equal("package-d", dep3.Pkgname, "depends package-d")
	assert.True(dep3.Conflicts, "depends package-d")
	assert.Equal("", dep3.Constraint, "depends package-d")
	assert.Equal("", dep3.RepoPin, "depends package-d")
	assert.Equal("", dep3.Version, "depends package-d")

	dep4 := pkg.Depends[3]
	assert.Equal("package-e", dep4.Pkgname, "depends package-e")
	assert.False(dep4.Conflicts, "depends package-e")
	assert.Equal("", dep4.Constraint, "depends package-e")
	assert.Equal("repo", dep4.RepoPin, "depends package-e")
	assert.Equal("", dep4.Version, "depends package-e")

	require.Len(pkg.DependsDev, 1, "depends_dev")
	assert.Equal("package-f-dev", pkg.DependsDev[0].Pkgname, "depends_dev")
	require.Len(pkg.Makedepends, 1, "makedepends")
	assert.Equal("package-g", pkg.Makedepends[0].Pkgname, "makedepends")
	require.Len(pkg.MakedependsBuild, 1, "makdepends_build")
	assert.Equal("package-h", pkg.MakedependsBuild[0].Pkgname, "makedepends_build")
	require.Len(pkg.MakedependsHost, 1, "makedepends_host")
	assert.Equal("package-i", pkg.MakedependsHost[0].Pkgname, "makedepends_host")
	require.Len(pkg.Checkdepends, 1, "checkdepends")
	assert.Equal("package-j", pkg.Checkdepends[0].Pkgname, "checkdepends")

	assert.Len(pkg.Subpackages, 3)
	assert.Equal("package-a-dbg", pkg.Subpackages[0].Subpkgname, "subpackages dbg")
	assert.Equal("package-a-doc", pkg.Subpackages[1].Subpkgname, "subpackages doc")
	assert.Equal("package-a-sub-pkg", pkg.Subpackages[2].Subpkgname, "subpackages sub-pkg")
	assert.Equal("sub", pkg.Subpackages[2].SplitFunc, "subpackages sub-pkg")
	assert.Equal("noarch", pkg.Subpackages[2].Arch, "subpackages sub-pkg")

	assert.Len(pkg.Options, 2)
	assert.True(pkg.Options.Has("net"), "expect options to have 'net'")
	assert.True(pkg.Options.Has("!check"), "expect options to have '!check'")

	require.Len(pkg.Source, 1)
	assert.Equal("package-a-1.2.0.tar.gz", pkg.Source[0].Filename, "source")
	assert.Equal("https://package-a.org/1.2.0.tar.gz", pkg.Source[0].Location, "source")

	require.Len(pkg.Provides, 1)
	assert.Equal("package-k", pkg.Provides[0].Pkgname, "provides")
	assert.Equal("100", pkg.ProviderPriority, "provider_priority")

	require.Len(pkg.Replaces, 1)
	assert.Equal("package-k", pkg.Replaces[0].Pkgname, "replaces")

	require.Len(pkg.Sha512sums, 1)
	assert.Equal(
		"32de73bec5e2a522b0bb22dda723f31aabee6ed3cb3bab9646f7b598662732d5f9579441bf04607187b99eb054f33db78220176b0fc0cb7a7ee3ce70917f28ff",
		pkg.Sha512sums[0].Hash,
		"sha512sums",
	)
	assert.Equal("package-a-1.2.0.tar.gz", pkg.Sha512sums[0].Source, "sha512sums")
}
