package apkbuild

import (
	"bufio"
	"bytes"
	"fmt"
	"io"
	"strings"

	"gopkg.in/yaml.v3"
)

type secfixesData struct {
	Secfixes   Secfixes
	lineOffset uint
}

var _ yaml.Unmarshaler = (*secfixesData)(nil)

func (s *secfixesData) UnmarshalYAML(node *yaml.Node) error {
	if node.Kind != yaml.MappingNode {
		return fmt.Errorf("expected node to be a '!!map', received %s", node.Tag)
	}

	if len(node.Content) < 2 {
		return fmt.Errorf("expected node to have 2 elements, but has %d", len(node.Content))
	}

	if node.Content[1].Kind != yaml.MappingNode {
		return fmt.Errorf("expected node to contain a !!map, received %s", node.Content[1].Tag)
	}

	var fixedVersion *FixedVersion
	for _, child := range node.Content[1].Content {
		switch child.Kind {
		case yaml.ScalarNode:
			fixedVersion = new(FixedVersion)
			fixedVersion.Version = child.Value
			fixedVersion.LineNr = uint(child.Line) + s.lineOffset
		case yaml.SequenceNode:
			var fixes []Fix
			err := child.Decode(&fixes)
			if err != nil {
				return err
			}
			for i := range fixes {
				fixes[i].LineNr += s.lineOffset
			}
			fixedVersion.Fixes = fixes
			s.Secfixes = append(s.Secfixes, *fixedVersion)
		}
	}
	return nil
}

type FixedVersion struct {
	Version string
	Fixes   []Fix
	LineNr  uint
}

type Fix struct {
	Identifiers []string
	LineNr      uint
}

func (f *Fix) UnmarshalYAML(node *yaml.Node) error {
	f.Identifiers = strings.Split(node.Value, " ")
	f.LineNr = uint(node.Line)
	return nil
}

type Secfixes []FixedVersion

func (s Secfixes) Get(version string) *FixedVersion {
	for _, fixedVersion := range s {
		if fixedVersion.Version == version {
			return &fixedVersion
		}
	}

	return nil
}

func ParseSecfixes(apkbuild io.ReadCloser) (secfixes Secfixes, err error) {
	defer apkbuild.Close()
	apkbuildScanner := bufio.NewScanner(apkbuild)

	var apkbuildLineNr uint
	var lineNr uint
	secfixYaml := bytes.Buffer{}
	inSecFixes := false

scanLoop:
	for apkbuildScanner.Scan() {
		line := apkbuildScanner.Text()
		apkbuildLineNr++

		switch {
		case inSecFixes && (len(line) == 0 || line[0:1] != "#"):
			break scanLoop
		case line == "# secfixes:":
			lineNr = apkbuildLineNr
			inSecFixes = true
			fallthrough
		case inSecFixes:
			if len(line) > 1 {
				secfixYaml.WriteString(line[2:])
			}
			secfixYaml.WriteByte('\n')
		}
	}

	secfixesData := secfixesData{}
	secfixesData.lineOffset = lineNr - 1
	err = yaml.Unmarshal(secfixYaml.Bytes(), &secfixesData)

	return secfixesData.Secfixes, err
}
