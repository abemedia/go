package releases

import (
	"io"
	"os"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"golang.org/x/exp/maps"
)

func TestNewFromBuffer(t *testing.T) {
	assert := assert.New(t)
	require := require.New(t)

	file, err := os.Open("testdata/releases.json")
	require.Nil(err)

	buffer, err := io.ReadAll(file)
	require.Nil(err)

	releases, err := NewFromBuffer(buffer)
	require.Nil(err)

	assert.Len(releases.Architectures, 2)
	require.Len(releases.ReleaseBranches, 2)

	firstBranch := releases.ReleaseBranches[0]
	assert.Equal("edge", firstBranch.RelBranch)
	assert.Equal("master", firstBranch.GitBranch)
	assert.Len(firstBranch.Repos, 3)

	secondBranch := releases.ReleaseBranches[1]
	assert.Equal("v3.13", secondBranch.RelBranch)
	assert.Equal("3.13-stable", secondBranch.GitBranch)
	assert.Equal("2022-11-01", secondBranch.Repos[1].EOLDate)
}

func TestNewFromBufferParsesKeys(t *testing.T) {
	assert := assert.New(t)
	require := require.New(t)

	file, err := os.Open("testdata/releases.json")
	require.Nil(err)

	releases, err := NewFromReader(file)
	require.Nil(err)

	firstBranch := releases.ReleaseBranches[0]
	assert.Len(
		firstBranch.Keys,
		2,
		"expected 2 keys in the first release branch, present: %s",
		strings.Join(maps.Keys(firstBranch.Keys), ", "),
	)

	assert.Contains(firstBranch.Keys, "x86_64", "expect edge branch to have keys for x86_64")
	x86_64 := firstBranch.Keys["x86_64"]
	require.Len(x86_64, 2, "expect edge/x86_64 to have 2 keys")
	assert.Equal("https://alpinelinux.org/keys/key1.pub", x86_64[0].Url)
	assert.Empty(x86_64[0].DeprecatedSince)
	assert.Equal("https://alpinelinux.org/keys/key2.pub", x86_64[1].Url)
	assert.Equal("2022-05-12", x86_64[1].DeprecatedSince)

	assert.Contains(firstBranch.Keys, "aarch64", "expect edge branch to have keys for aarch64")
	aarch64 := firstBranch.Keys["aarch64"]
	require.Len(aarch64, 1, "expect edge/aarch64 to have 1 key")
	assert.Equal("https://alpinelinux.org/keys/key3.pub", aarch64[0].Url)
	assert.Empty(aarch64[0].DeprecatedSince)
}
